@AfterEach
Feature: Create Account Feature
Scenario: Account creation
    Given I am in Sign In page
    When I enter email in Create New Account section
    And I enter valid account details
    And click on Register button
    And My Account page is opened
    And I click on My Personal Information button
    Then Your Personal Information page is opened
    And correct personal information is displayed