package stepsDefinations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.*;

public class AddItemToWishlist {

    private NavigationSteps navigate = new NavigationSteps();
    private AuthenticationPage authenticationPage = new AuthenticationPage();
    private CreateAnAccountPage createAnAccountPage = new CreateAnAccountPage();
    private MyAccountPage myAccountPage = new MyAccountPage();
    private YourPersonalInformationPage yourPersonalInformationPage = new YourPersonalInformationPage();
    private LandingPage landingPage = new LandingPage();



    @When("I click on BEST SELLERS button")
    public void iClickOnBESTSELLERSButton() {
    }

    @And("I select first item from list")
    public void iSelectFirstItemFromList() {
    }

   @And("item description and conditions are displayed")
   public void itemDescriptionAndConditionsAreDisplayed() {
   }

    @And("I set size to L")
    public void iSetSizeToL() {

    }

    @And("I set color to Green")
    public void iSetColorToGreen() {

    }

    @And("I click on Add to wishlist button")
    public void iClickOnAddToWishlistButton() {

    }

    @And("I open My Whishlist page")
    public void iOpenMyWhishlistPage() {

    }

    @And("I click on My Wishlist")
    public void iClickOnMyWishlist() {
    }

    @Then("My Wishlist item are visible")
    public void myWishlistItemAreVisible() {
    }

    @And("correct wishlist item preferences is displayedl")
    public void correctWishlistItemPreferencesIsDisplayedl() {

    }

}
