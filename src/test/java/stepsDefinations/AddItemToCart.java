package stepsDefinations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.BeforeAll;
import pages.*;

public class AddItemToCart {

    private NavigationSteps navigate = new NavigationSteps();
    private MyAccountPage myAccountPage = new MyAccountPage();
    private ItemsPage itemsPage = new ItemsPage();
    private ShoppingChartSummaryPage shoppingChartSummaryPage = new ShoppingChartSummaryPage();
    private AuthenticationPage authenticationPage = new AuthenticationPage();
    private LandingPage landingPage = new LandingPage();
    private CreateAnAccountPage createAnAccountPage = new CreateAnAccountPage();

    @Given("I am in Landing page")
    public void iAmInLandingPage() {
        landingPage.validateLandingPage();
    }

    @Given("I am logged in")
    public void iAmLoggedIn() {
        navigate.navigateToAuthenticationPage();
        authenticationPage.enterEmail();
        navigate.navigateToCreateAnAccountPage();
        createAnAccountPage.enterValidCustomerDetails();
        navigate.navigateToMyAccountPage();
    }

    @When("I select Search menu")
    public void iSelectSearchMenu() {
        myAccountPage.selectSearchScreen();
    }

    @And("I enter {string}")
    public void iEnter(String item) {
        myAccountPage.fillSearchScreen(item);

    }

    @And("I click on Search button")
    public void iClickOnSearchButton() {
        myAccountPage.selectSearchButton();
    }

    @And("only {int} result is found")
    public void onlyResultIsFound(int count) {
        myAccountPage.validateSearchResults(count);
    }

    @And("I select the item")
    public void iSelectTheItem() {
        navigate.navigateToItemsPage();
    }

    @And("item description and condition is displayed")
    public void itemDescriptionAndConditionIsDisplayed() {
        itemsPage.validateItemScreen();
    }

    @And("I change quantity to {int}")
    public void iChangeQuantityTo(int Quantity) {
        itemsPage.setgQuantityWantedValue(Quantity);
    }

    @And("I set size to S")
    public void iSetSizeToS() {
        itemsPage.setSizeValue();
    }

    @And("I set color to White")
    public void iSetColorToWhite() {
        itemsPage.setColourWhite();
    }

    @And("I select Add to cart button")
    public void iSelectAddToCartButton() {
        itemsPage.selectAddToCartButton();
    }

    @And("item is successfully added to cart")
    public void itemIsSuccessfullyAddedToCart() {
        itemsPage.validateAprooveMessage();
    }

    @And("I click on Proceed to checkout button")
    public void iClickOnProceedToCheckoutButton() {
        navigate.navigateToShoppingChartSummaryPage();
    }

    @Then("Shopping cart summary page is opened")
    public void shoppingCartSummaryPageIsOpened() {
        shoppingChartSummaryPage.validateSumaryPageHeader();
    }

    @And("correct description is specified")
    public void correctDescriptionIsSpecified() {
        shoppingChartSummaryPage.validatgeQuantity();
    }

    @And("amount is correctly calculated")
    public void amountIsCorrectlyCalculated() {
        shoppingChartSummaryPage.validateChartTotal();
    }

    @And("Proceed to checkout button is visible")
    public void proceedToCheckoutButtonIsVisible() {
        shoppingChartSummaryPage.validateProcedeButton();
    }


}
