package stepsDefinations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import pages.*;

public class CreateAccountSteps {
    private NavigationSteps navigate = new NavigationSteps();
    private AuthenticationPage authenticationPage = new AuthenticationPage();
    private CreateAnAccountPage createAnAccountPage = new CreateAnAccountPage();
    private MyAccountPage myAccountPage = new MyAccountPage();
    private YourPersonalInformationPage yourPersonalInformationPage = new YourPersonalInformationPage();

    @Given("I am in Sign In page")
    public void iAmInSignInPage() {
        navigate.navigateToAuthenticationPage();

    }

    @When("I enter email in Create New Account section")
    public void iEnterEmailInCreateNewAccountSection() {
        authenticationPage.enterEmail();
        navigate.navigateToCreateAnAccountPage();
    }

    @And("I enter valid account details")
    public void iEnterValidAccountDetails() {
        createAnAccountPage.enterValidCustomerDetails();
    }

    @And("click on Register button")
    public void clickOnRegisterButton() {
        navigate.navigateToMyAccountPage();
    }

    @And("My Account page is opened")
    public void myAccountPageIsOpened() {
        myAccountPage.validateMyAccountPage();

    }

    @And("I click on My Personal Information button")
    public void iClickOnMyPersonalInformationButton() {
        navigate.navigateToYourPersonalInformationPage();
    }

    @Then("Your Personal Information page is opened")
    public void yourPersonalInformationPageIsOpened() {
        yourPersonalInformationPage.validateYourPersonalInformationPage();
    }

    @And("correct personal information is displayed")
    public void correctPersonalInformationIsDisplayed() {
        yourPersonalInformationPage.validatePersonalData();
    }
}
