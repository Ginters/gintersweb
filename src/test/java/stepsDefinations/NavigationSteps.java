package stepsDefinations;

import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.en.Given;
import cucumber.api.java.eo.Se;
import pages.*;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class NavigationSteps {

    private SelenideElement getSignInButton(){
        return $(".login");
    }

    private SelenideElement getCreateAccountButton(){
        return $("#SubmitCreate");
    }

    private SelenideElement getSubmitAccountButton(){
        return $("#submitAccount");
    }

    private SelenideElement getSearchResults() {
        return $("[class='product_list grid row'] [class='product-name']");
    }


    private SelenideElement getMyPersonalData(){
        return $("[class='row addresses-lists'] [title='Information']");
    }

    private SelenideElement getProceedButton() {
        return $("[class='btn btn-default button button-medium']");
    }

    private  SelenideElement getSubmitButton() {
        return $("#SubmitLogin");
    }





    public AuthenticationPage navigateToAuthenticationPage(){
        getSignInButton().click();
        return page(AuthenticationPage.class);
    }

    public CreateAnAccountPage navigateToCreateAnAccountPage(){
        getCreateAccountButton().click();
        return page(CreateAnAccountPage.class);
    }

    public MyAccountPage navigateToMyAccountPage(){
        getSubmitAccountButton().click();
        return page(MyAccountPage.class);
    }

    public YourPersonalInformationPage navigateToYourPersonalInformationPage(){
        getMyPersonalData().click();
        return page(YourPersonalInformationPage.class);
    }

    public ItemsPage navigateToItemsPage() {
        getSearchResults().click();
        return page(ItemsPage.class);
    }

    public ShoppingChartSummaryPage navigateToShoppingChartSummaryPage() {
        getProceedButton().click();
        return  page(ShoppingChartSummaryPage.class);
    }

    public MyAccountPage navigateToMyAccountPageWithExistingCreddentials() {
        getSubmitButton().click();
        return page(MyAccountPage.class);
    }



}
