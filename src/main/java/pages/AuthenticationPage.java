package pages;

import com.codeborne.selenide.SelenideElement;
import utils.RandomHelper;
import utils.User;

import static com.codeborne.selenide.Selenide.$;

public class AuthenticationPage {

    private SelenideElement getHeader(){
        return $(".page-heading");
    }

    private SelenideElement getEmailField(){
        return $("#email_create");
    }

    private SelenideElement getEmaiExistinglField() {
        return $("#email");
    }

    private SelenideElement getPasswoardField() {
        return $("#passwd");
    }

    public void enterEmail(){
        String email = User.email();
        getEmailField().sendKeys(email);
    }

    public void enterValidEmail() {
        String email = User.email();
        getEmaiExistinglField().sendKeys(email);
    }

    public void enterValidPasswoard() {
        String passw = User.passw();
        getPasswoardField().sendKeys(passw);
    }

}
