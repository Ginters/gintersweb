package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ShoppingChartSummaryPage {

    private SelenideElement getSumaryPageHeader(){
         return $("#cart_title");
    }

    private SelenideElement getQtyDetails() {
        return $("[class='cart_quantity_input form-control grey']");
    }

    private SelenideElement getUnitPrice() {
        return $("[data-title='Unit price'] > [class='price'] ");

    }

    private SelenideElement getCartTotal() {
        return $("[class='cart_total'] > [class='price'] ");
    }

    private SelenideElement getCartQuantity() {
        return $("[class='cart_quantity_input form-control grey']");
    }

    private SelenideElement getProceedeButton() {
        return $("[class='cart_navigation clearfix'] [title='Proceed to checkout']");
    }

    public void validateSumaryPageHeader(){
        String header = getSumaryPageHeader().getText();
        header.equals("Shopping-cart summary");
    }

    public void validatgeQuantity() {
        String quantity = getQtyDetails().getValue();
        quantity.equals("2");
    }

    public void validateChartTotal(){
        String unitPrice = getUnitPrice().getText();
        unitPrice = unitPrice.replaceAll("\\D+","");
        int unit = Integer.parseInt(unitPrice);

        String totalPrice = getCartTotal().getText();
        totalPrice = totalPrice.replaceAll("\\D+","");
        int total = Integer.parseInt(totalPrice);

        String itemCount = getCartQuantity().getValue();
        itemCount = itemCount.replaceAll("\\D+","");
        int count = Integer.parseInt(itemCount);

        int price = unit * count;
        if (total == price) {
           System.out.println("Price check PASS");
        }else{
           System.out.println("Price check FAILS");
       }
    }

    public void validateProcedeButton() {
        getProceedeButton().isDisplayed();
    }

}
