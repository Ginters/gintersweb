package pages;

import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.eo.Se;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;

public class ItemsPage {

    private SelenideElement getItemConditions(){
        return $("#product_condition");
    }
    private SelenideElement getQuantityWantedField(){
        return $("#quantity_wanted");
    }

    private SelenideElement getSizeValue() {
       return  $("[id='group_1'] [value='1']");
    }

    private SelenideElement getWhiteColour() {
        return $("#color_8");
    }

    private SelenideElement getAddToCartButton() {
        return $("#add_to_cart > button > span");
    }

    private SelenideElement getIconOkHeader() {
        return $(".icon-ok");
    }

    public void validateItemScreen() {
        getItemConditions().isDisplayed();
        String value = getQuantityWantedField().getValue();
        value.equals("1");
    }

    public void setgQuantityWantedValue(int Quantity) {
        getQuantityWantedField().setValue(String.valueOf(Quantity));
    }

    public void setSizeValue() {
        getSizeValue().click();
    }

    public void setColourWhite() {
        getWhiteColour().click();
    }

    public void selectAddToCartButton() {
        getAddToCartButton().click();
    }

    public void validateAprooveMessage() {
       String message = getIconOkHeader().getText();
       message.equals("Product successfully added to your shopping cart");

    }


}
