package pages;

import com.codeborne.selenide.SelenideElement;
import utils.User;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.$;
import static utils.User.name;

public class YourPersonalInformationPage {

    private SelenideElement getSubheading() {
        return $(".page-subheading");
    }

    private SelenideElement getFirstNameField() {
        return $("#firstname");
    }

    private SelenideElement getLastNameField() {
        return $("#lastname");
    }

    private SelenideElement getEmailField() {
        return $("#email");
    }

    public void  validateYourPersonalInformationPage(){
        String header = getSubheading().getText();
        header.equals("Your personal information");
    }

    public void validateNameField() {
        String firstName = getFirstNameField().getValue();
        firstName.equals(User.name());
    }

    public void validateLastNameField() {
        String lastName = getLastNameField().getValue();
        lastName.equals(User.lastName());
    }

    public void validateEmailField() {
        String emailaddress = getLastNameField().getValue();
        emailaddress.equals(User.email());
    }

    public void validatePersonalData() {
        validateNameField();
        validateLastNameField();
        validateEmailField();
    }

}
