package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LandingPage {
    private SelenideElement getLandingPage(){
        return $("#header_logo");
    }


    public void validateLandingPage(){
        getLandingPage().isDisplayed();
    }

}
