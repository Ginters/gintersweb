package pages;

import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.eo.Se;

import static com.codeborne.selenide.Selenide.$;

public class MyAccountPage {

    private SelenideElement getPageHeader(){
        return $(".page-heading");
    }

    private SelenideElement getSearchScreen() {
        return $("#search_query_top");
    }

    private SelenideElement getSearchButton() {
        return $("[name='submit_search']");
    }

    private SelenideElement getSearchCount() {
        return $(".product-count");
    }






    public void validateMyAccountPage(){
       String header =  getPageHeader().getText();
       header.equals("My account");
    }

    public void selectSearchScreen() {
        getSearchScreen().click();
    }

    public void fillSearchScreen(String item) {
        getSearchScreen().setValue(item);
    }

    public void selectSearchButton() {
        getSearchButton().click();
    }

    public void validateSearchResults(int count){
        String itemCount = getSearchCount().getText();
        itemCount.equals("Showing 1 - 1 of " + count + " item");
    }





}
