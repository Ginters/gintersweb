package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Dimension;
import utils.RandomHelper;
import utils.User;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;

public class CreateAnAccountPage {

    private SelenideElement getCustomerNameField() {
        return $("#customer_firstname");
    }

    private SelenideElement getCustomerLastNameField() {
        return $("#customer_lastname");
    }

    private SelenideElement getCustomerPasswordField() {
        return $("#passwd");
    }

    private SelenideElement getAddressNameField() {
        return $("[class='required form-group'] [id='firstname']");
    }

    private SelenideElement getAdresLastNameField() {
        return $("#lastname");
    }

    private SelenideElement getAddressField() {
        return $("#address1");
    }

    private SelenideElement getCityField() {
        return $("#city");
    }

    private SelenideElement getStateDropdown() {
        return $("#id_state");
    }

    private SelenideElement getDropdownValue() {
        int rand = (int) Math.random();
        rand = rand * 50 + 1;
        String random = String.valueOf(rand);
        return $("[id='id_state'] [value='" + random + "']");
    }

    private SelenideElement getPostCodeField() {
        return $("#postcode");
    }

    private SelenideElement getMobilePhoneField() {
        return $("#phone_mobile");
    }
    private SelenideElement getAliasField() {
        return $("#alias");
    }


    public void enterCustomerName() {
        String name = User.name();
        getCustomerNameField().sendKeys(name);
    }

    public void enterCustomerLastName() {
        String lastName = User.lastName();
        getCustomerLastNameField().sendKeys(lastName);
    }

    public void enterCustomerPassword() {
        String password = User.passw();
        getCustomerPasswordField().sendKeys(password);
    }

    public void enterAddressFirstName(){
        String addressFirstName = RandomHelper.generateRandomString(8,true,false);
        getAddressNameField().sendKeys(addressFirstName);
    }

    public void enterAddressName() {
        String addressName = RandomHelper.generateRandomString(8,true,false);
        getAdresLastNameField().sendKeys(addressName);
    }

    public void enterAddress() {
        String address = RandomHelper.generateRandomString(8,true,false);
        getAddressField().sendKeys(address);
    }

    public void enterCity() {
        String city = RandomHelper.generateRandomString(8,true,false);
        getCityField().sendKeys(city);
    }

    public void enterStateDropdownValue() {
        getStateDropdown().click();
        getDropdownValue().click();


    }

    public void enterPostCode() {
        String postCode = RandomHelper.generateRandomString(5,false,true);
        getPostCodeField().sendKeys(postCode);
    }

    public void enterMobilePhone() {
        String phoneNumber = RandomHelper.generateRandomString(8,false,true);
        getMobilePhoneField().sendKeys(phoneNumber);
    }

    public void entergetAlias() {
        String alias = RandomHelper.generateRandomString(8,true,false);
        getAliasField().sendKeys(alias);
    }
    public void enterValidCustomerDetails() {
        enterCustomerName();
        enterCustomerLastName();
        enterCustomerPassword();
        enterAddressFirstName();
        enterAddressName();
        enterAddress();
        enterCity();
        enterStateDropdownValue();
        enterPostCode();
        enterMobilePhone();
        entergetAlias();
    }

}

