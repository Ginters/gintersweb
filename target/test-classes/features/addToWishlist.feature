@skip_scenario
Feature: Adding item to Whislist Feature
Scenario: Adding item to Whislist
    Given I am logged in
    And I am in Landing page
    When I click on BEST SELLERS button
    And I select first item from list
    And item description and conditions are displayed
    And I set size to L
    And I set color to Green
    And I click on Add to wishlist button
    And I open My Whishlist page
    And I click on My Wishlist
    Then My Wishlist item are visible
    And correct wishlist item preferences is displayedl